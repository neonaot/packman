#include "lcd_22.h"
#include <stdio.h>
#include <stdlib.h>
#include <bcm2835.h>


//объявление адресов для связи с кнопками
#define KEY0 RPI_GPIO_P1_11
#define KEY1 RPI_GPIO_P1_12
#define KEY2 RPI_V2_GPIO_P1_13
#define KEY3 RPI_GPIO_P1_15
#define KEY4 RPI_GPIO_P1_16
#define KEY5 RPI_GPIO_P1_18
#define KEY6 RPI_GPIO_P1_22
#define KEY7 RPI_GPIO_P1_07

#define EPS 10 //погрешность попадания в цель

char KEY[] = {KEY0, KEY1, KEY2, KEY3, KEY4, KEY5, KEY6, KEY7};



void LCD_WRITE_REG(unsigned int index)
{
  unsigned int value_index;
  en_lcd_index();//LCD_RS_L();
  en_lcd();//LCD_CS_L();
  value_index = index;
  value_index = value_index >> 8;
  bcm2835_spi_transfer((unsigned char) (value_index));    //00000000 000000000

  value_index = index;
  value_index &= 0x00ff;
  bcm2835_spi_transfer((unsigned char) (index));
  dis_lcd();//LCD_CS_H();
  en_lcd_data();//LCD_RS_H();
}

void LCD_WRITE_DATA(unsigned int data)
{
  bcm2835_spi_transfer((unsigned char) (data >> 8));    
  bcm2835_spi_transfer((unsigned char) (data));
}

void LCD_SEND_COMMAND(unsigned int index, unsigned int data)
{
  //select command register
  en_lcd_index();//LCD_RS_L();
  en_lcd();//LCD_CS_L();
  bcm2835_spi_transfer((unsigned char) (index >> 8));   
  bcm2835_spi_transfer((unsigned char) (index));
  dis_lcd();//LCD_CS_H();
  //send data
  en_lcd_data();//LCD_RS_H();
  en_lcd();//LCD_CS_L();
  bcm2835_spi_transfer((unsigned char) (data >> 8));  
  bcm2835_spi_transfer((unsigned char) (data));
  dis_lcd();//LCD_CS_H();
}


void lcd_draw_dot(unsigned int color_front,
                  unsigned int x,
                  unsigned int y, int r)
{

  LCD_SEND_COMMAND(0x210, x);
  LCD_SEND_COMMAND(0x212, y);
  LCD_SEND_COMMAND(0x211, x + r);
  LCD_SEND_COMMAND(0x213, y + r);

  LCD_SEND_COMMAND(0x200, x +r );
  LCD_SEND_COMMAND(0x201, y +r );

  en_lcd_index();//LCD_RS_L();
  LCD_WRITE_REG(0x202);   //RAM Write index
  en_lcd();//LCD_CS_L();
  LCD_WRITE_DATA(color_front);
  dis_lcd();


}

void lcd_clear_screen(unsigned int color_background)
{
  post_cmd(0x210,0x00);
  post_cmd(0x212,0x0000);
  post_cmd(0x211,0xEF);
  post_cmd(0x213,0x013F);

  post_cmd(0x200,0x0000);
  post_cmd(0x201,0x0000);

  en_lcd_index();
  post_data(0x202);
  en_lcd_data();
  int temp=color_background;
  for(int i=0;i<240;i++)
  {
    for(int num=0;num<320;num++)
    {
      post_data(temp);
    }
  }

  dis_lcd();
}



void draw_square(unsigned int color, int radius, unsigned int position_x, unsigned int position_y)
{
  int i,j;
  for (i=0; i<=2*radius; i++)
  {
    for (j=0; j<=2*radius; j++)
    {
      
        lcd_draw_dot(color, i+position_x, j+position_y, 3);
    }

  }

}


typedef struct packman {
  unsigned int xpos;
  unsigned int ypos;
  unsigned int r;
  unsigned int level;

  unsigned int V;

  unsigned int Fx;
  unsigned int Fy;

  unsigned int dx;
  unsigned int dy;
} packman_t;

void dCalc(struct packman p, int xx, int yy)
{
  p.dx = -(p.xpos - xx);
  p.dy = -(p.ypos - yy);
}

void drawPackman(packman_t p)
{
  draw_square(0xffe0,p.r,  p.xpos, p.ypos);
};

void drawF(packman_t p)
{
  draw_square(0xF800, 3, p.Fx, p.Fy);

};

unsigned int GetTouchADC (unsigned char data)
{
  u8 tmp1,tmp2;
  u16  tmp;
  dis_lcd();
  en_touch();
  delayMicroseconds(2);
  bcm2835_spi_transfer(data);
  delayMicroseconds(2);
  tmp1=bcm2835_spi_transfer(0x00);
  tmp2=bcm2835_spi_transfer(0x00);
  tmp=((u16)(tmp1)<<5 | (u16)(tmp2)>>3 );
  dis_touch();
  return (tmp);
/*
        unsigned int NUMH , NUML;
        unsigned int Num;
        dis_lcd();//LCD_CS_H()
        en_touch();//TOUCH_nCS_L();

        delayMicroseconds(2);
        bcm2835_spi_transfer(data);
        delayMicroseconds(2);              // ÑÓÊ±µÈ´ý×ª»»Íê³É
        NUMH=bcm2835_spi_transfer(0x00);
        NUML=bcm2835_spi_transfer(0x00);
        Num=((NUMH)<<8)+NUML;
        Num>>=4;                // Ö»ÓÐ¸ß12Î»ÓÐÐ§.
        dis_touch();//TOUCH_nCS_H();

        return(Num);
*/
}
typedef struct
{
  unsigned int  x;//LCD×ø±ê
  unsigned int  y;
  unsigned long x_ad_val; //ADCÖµ
  unsigned long y_ad_val;
  unsigned char  pen_status;//±ÊµÄ×´Ì¬
}_touch_dot;


_touch_dot touch_dot,*p_touch_dot;

#define READ_TIMES 10 //
#define LOST_VAL 4        //

unsigned int GetTouchADCEx(unsigned char cmd_code)
{
  unsigned int i, j;
  unsigned int buf[READ_TIMES];
  unsigned int sum=0;
  unsigned int temp;

  for(i=0;i<READ_TIMES;i++)
  {
    buf[i]=GetTouchADC(cmd_code);
  }
  for(i=0;i<READ_TIMES-1; i++)//ÅÅÐò
  {
    for(j=i+1;j<READ_TIMES;j++)
    {
      if(buf[i]>buf[j])//ÉýÐòÅÅÁÐ
      {
        temp=buf[i];
        buf[i]=buf[j];
        buf[j]=temp;
      }
    }
  }
  sum=0;
  for(i=LOST_VAL;i<READ_TIMES-LOST_VAL;i++)sum+=buf[i];
  temp=sum/(READ_TIMES-2*LOST_VAL);
  return temp;
}

unsigned char Read_ADS(unsigned int *x_ad,unsigned int *y_ad)
{
  unsigned int xtemp,ytemp;
  xtemp=GetTouchADCEx(TOUCH_CMD_X);    //ÓÐÉ¸Ñ¡µÄ¶ÁÈ¡XÖáAD×ª»»½á¹û
  ytemp=GetTouchADCEx(TOUCH_CMD_Y);            //ÓÐÉ¸Ñ¡µÄ¶ÁÈ¡YÖáAD×ª»»½á¹û
  if(xtemp<100||ytemp<100)
    return 1;   //¶ÁÊýÊ§°Ü
  *x_ad=xtemp;
  *y_ad=ytemp;
  return 0;//¶ÁÊý³É¹¦
}

#define ERR_RANGE 50 //Îó²î·¶Î§

unsigned char Read_ADS2(unsigned long *x_ad,unsigned long *y_ad)
{
  unsigned int x1,y1;
  unsigned int x2,y2;
  unsigned char res;

  res=Read_ADS(&x1,&y1);  // µÚÒ»´Î¶ÁÈ¡ADCÖµ
  if(res==1)  return(1);      // Èç¹û¶ÁÊýÊ§°Ü£¬·µ»Ø1
  res=Read_ADS(&x2,&y2);      // µÚ¶þ´Î¶ÁÈ¡ADCÖµ
  if(res==1)  return(1);      // Èç¹û¶ÁÊýÊ§°Ü£¬·µ»Ø1
  if(((x2<=x1&&x1<x2+ERR_RANGE)||(x1<=x2&&x2<x1+ERR_RANGE))//Ç°ºóÁ½´Î²ÉÑùÔÚ+-50ÄÚ
     &&((y2<=y1&&y1<y2+ERR_RANGE)||(y1<=y2&&y2<y1+ERR_RANGE)))
  {
    *x_ad=(x1+x2)/2;
    *y_ad=(y1+y2)/2;
    return 0;        // ÕýÈ·¶ÁÈ¡£¬·µ»Ø0
  }
  else return 1;       // Ç°ºó²»ÔÚ+-50ÄÚ£¬¶ÁÊý´íÎó
}

void convert_ad_to_xy(void)
{
  touch_dot.x=((touch_dot.x_ad_val-121)/13.030); // °Ñ¶Áµ½µÄX_ADCÖµ×ª»»³ÉTFT X×ø±êÖµ
  touch_dot.y=((touch_dot.y_ad_val-101)/10.500); // °Ñ¶Áµ½µÄY_ADCÖµ×ª»»³ÉTFT Y×ø±êÖµ
}

unsigned char Read_Once(void)
{
//      touch_dot.pen_status=Pen_Up;
  if(Read_ADS2(&touch_dot.x_ad_val,&touch_dot.y_ad_val)==0)       // Èç¹û¶ÁÈ¡Êý¾Ý³É¹¦
  {
    while(((bcm2835_gpio_lev))==0);   // ¼ì²â±ÊÊÇ²»ÊÇ»¹ÔÚÆÁÉÏ:IRQÎªµÍµçÆ½(bit7Îª0)ËµÃ÷±ÊÔÚÆÁÉÏ
    convert_ad_to_xy();   // °Ñ¶Áµ½µÄADCÖµ×ª±ä³ÉTFT×ø±êÖµ
    return 0;       // ·µ»Ø0£¬±íÊ¾³É¹¦
  }
  else return 1;  // Èç¹û¶ÁÈ¡Êý¾ÝÊ§°Ü£¬·µ»Ø1±íÊ¾Ê§°Ü
}

unsigned char Read_Continue(void)
{
//      touch_dot.pen_status=Pen_Up;
  if(Read_ADS2( &touch_dot.x_ad_val,&touch_dot.y_ad_val )==0)      // Èç¹û¶ÁÈ¡Êý¾Ý³É¹¦
  {
    convert_ad_to_xy();   // °Ñ¶Áµ½µÄADCÖµ×ª±ä³ÉTFT×ø±êÖµ
    return 0;          // ·µ»Ø0£¬±íÊ¾³É¹¦
  }
  else return 1;     // Èç¹û¶ÁÈ¡Êý¾ÝÊ§°Ü£¬·µ»Ø1±íÊ¾Ê§°Ü
}
void LCD_WRITE_COMMAND(unsigned int index,unsigned int data)
{
  //select command register
  en_lcd_index();//LCD_RS_L();
  en_lcd();//LCD_CS_L();
  bcm2835_spi_transfer((unsigned char)(index>>8));    //00000000 000000000
  bcm2835_spi_transfer((unsigned char)(index));
  dis_lcd();//LCD_CS_H();
  //send data
  en_lcd_data();//LCD_RS_H();

  en_lcd()//LCD_CS_L();
  bcm2835_spi_transfer((unsigned char)(data>>8));    //00000000 000000000
  bcm2835_spi_transfer((unsigned char)(data));
  dis_lcd();//LCD_CS_H();
}




void lcd_draw_bigdot(unsigned int   color_front,
                     unsigned int    x,
                     unsigned int    y )
{
  lcd_draw_dot(color_front,x,y, 3);
  lcd_draw_dot(color_front,x,y+3, 3);
  lcd_draw_dot(color_front,x,y-3, 3);

  lcd_draw_dot(color_front,x+3,y, 3);
  lcd_draw_dot(color_front,x+3,y+3, 3);
  lcd_draw_dot(color_front,x+3,y-3, 3);

  lcd_draw_dot(color_front,x-3,y, 3);
  lcd_draw_dot(color_front,x-3,y+3, 3);
  lcd_draw_dot(color_front,x-3,y-3,3);

}

unsigned char read_res ;

void main(void)
{

  //в си нет классов! :*(
  packman_t game = {30, 30, 5, 5, 3, 45, 45, 0, 3};


  int i;
  bcm2835_init();


  for (i = 0; i < 8; i++) {
    bcm2835_gpio_fsel(KEY[i], BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_set_pud(KEY[i], BCM2835_GPIO_PUD_UP);
  }

  init_lcd(0);


  lcd_clear_screen(0x0000);


  int n = 0;
  while (1) {

    if (bcm2835_gpio_lev(KEY[3]) != 0) {

      n += 1;


      drawPackman(game);
      drawF(game);

      if (bcm2835_gpio_lev(KEY[2]) == 0) {
        game.dx = game.V;
        game.dy = 0;
      } else if (bcm2835_gpio_lev(KEY[0]) == 0) {
        game.dx = -game.V;
        game.dy = 0;
      } else if (bcm2835_gpio_lev(KEY[5]) == 0) {
        game.dx = 0;
        game.dy = game.V;
      } else if (bcm2835_gpio_lev(KEY[1]) == 0) {
        game.dx = 0;
        game.dy = -game.V;
      } else if (bcm2835_gpio_lev(KEY[6]) == 0) {
        break;
      }


      //проверка что мы нашли цель
      if (abs(game.Fx - game.xpos) < EPS && abs(game.Fy - game.ypos < EPS)) {
        game.r++;
        game.level++;
        game.V += game.level;
        printf("\n ura \n");
        draw_square(0x0000, 3, game.Fx, game.Fy);
        game.Fy = rand() % 320;
        game.Fx = rand() % 240;
      }

      //удаление старого колобка
      draw_square(0x0000, game.r, game.xpos, game.ypos);


      //сдвиг колобка
      game.xpos = (game.xpos + game.dx);
      game.ypos = (game.ypos + game.dy);

      //смерть при выходе за пределы поля

      if (game.xpos < 0 || game.xpos > 240 || game.ypos < 0 || game.ypos > 320) {
        printf("\nGame over \n");

        lcd_clear_screen(0xF800);

        break;
      }
    } else {
      u16 num;
      u16 *p;
      u8 c,g;

      init_lcd(1);
      printf("ctoto");
      LCD_test();

      while(1){
        //printf("\n while 1 \n");
        //display_touch_debug();
        read_res=Read_Continue();
        //printf("\n %d   %d \n", touch_dot.x, touch_dot.y);
        lcd_draw_bigdot(0xFFFF,(touch_dot.x),(touch_dot.y));

      }

    }



  }
  bcm2835_close();
};
